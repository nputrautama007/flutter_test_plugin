import Flutter
import UIKit

public class SwiftFlutterTestPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_test_plugin", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterTestPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch (call.method) {
        case "getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
            break;
        case "generateRandomColor":
            let generateColor = generateRandomColor();
            result(generateColor)
            break;
        default:
            result("Not Implemented!")
            break;
        }
        
    }
    
    private func generateRandomColor() -> [Int]{
        return [0,0,0].map{
            (value) -> Int in
            return Int.random(in: 0..<256)
        }
    }
}
