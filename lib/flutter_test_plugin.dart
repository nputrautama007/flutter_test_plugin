import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FlutterTestPlugin {
  static const MethodChannel _channel =
      const MethodChannel('flutter_test_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<Color> generateRandomColors() async {
    final randomColor = await _channel.invokeListMethod('generateRandomColor');

    return Color.fromRGBO(randomColor[0], randomColor[1], randomColor[2], 1.0);
  }
}
